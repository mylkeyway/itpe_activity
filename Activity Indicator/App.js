import React from 'react';
import { AppRegistry, View } from 'react-native';

import Activity from './ITPE_Activity/ActivityIndicator';

export default class App extends React.Component {
  render() {
    return (

      <View>
        <Activity/>
      </View>

    );
  }
}

AppRegistry.registerComponent('App', ()=> App);
